package iuh.fit.www.week6_onthi;

import iuh.fit.www.week6_onthi.model.Address;
import iuh.fit.www.week6_onthi.model.Candidate;
import iuh.fit.www.week6_onthi.service.AddressService;
import iuh.fit.www.week6_onthi.service.CandidateService;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Candidate> list = new CandidateService().findAll();
        AddressService addressService = new AddressService();

        for (Candidate candidate: list) {
            System.out.println(candidate.getPhone());
            System.out.println(addressService.findAllAddress());
            Address address = new Address("2","2",2,"2","2");
            addressService.insertAddress(address);
        }
    }
}
