package iuh.fit.www.week6_onthi.service;

import iuh.fit.www.week6_onthi.model.Address;
import iuh.fit.www.week6_onthi.repository.AddressRepository;

import java.util.List;

public class AddressService {
    private AddressRepository addressRepository;

    public AddressService() {
      addressRepository = new AddressRepository();
    }

    public void insertAddress (Address address) {
        addressRepository.insertAddress(address);
    }
    public List<Address> findAllAddress(){
        return addressRepository.findAll();
    }
}
