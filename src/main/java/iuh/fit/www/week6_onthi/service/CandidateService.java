package iuh.fit.www.week6_onthi.service;

import iuh.fit.www.week6_onthi.model.Candidate;
import iuh.fit.www.week6_onthi.repository.CandidateRepository;

import java.util.List;

public class CandidateService {
    private CandidateRepository candidateRepository;

    public CandidateService() {
        candidateRepository = new CandidateRepository();
    }

    public List<Candidate> findAll() {
        return candidateRepository.findAll();
    }
}
