package iuh.fit.www.week6_onthi.model;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

import java.io.Serializable;

@Embeddable
public class CandidateSkillKey implements Serializable {
    @Column(name = "can_id")
    long canId;

    @Column(name = "skill_id")
    long skillId;
}
