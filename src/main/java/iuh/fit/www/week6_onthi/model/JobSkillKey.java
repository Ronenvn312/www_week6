package iuh.fit.www.week6_onthi.model;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

import java.io.Serializable;

@Embeddable
public class JobSkillKey implements Serializable {

    @Column(name = "job_id")
    long jobId;

    @Column(name = "skill_id")
    long skillId;

}
