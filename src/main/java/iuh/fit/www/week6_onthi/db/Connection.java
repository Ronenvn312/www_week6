package iuh.fit.www.week6_onthi.db;

import iuh.fit.www.week6_onthi.model.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class Connection {

    private static Connection instance;

    private SessionFactory sessionFactory;

    private Connection() {
        StandardServiceRegistry standardServiceRegistry = new StandardServiceRegistryBuilder().configure().build();
        Metadata metadata = new MetadataSources(standardServiceRegistry)
                .addAnnotatedClass(Address.class)
                .addAnnotatedClass(Candidate.class)
                .addAnnotatedClass(CandidateSkill.class)
                .addAnnotatedClass(CandidateSkillKey.class)
                .addAnnotatedClass(Company.class)
                .addAnnotatedClass(Job.class)
                .addAnnotatedClass(JobSkill.class)
                .addAnnotatedClass(JobSkillKey.class)
                .addAnnotatedClass(Skill.class)
                .getMetadataBuilder().build();
        sessionFactory = metadata.getSessionFactoryBuilder().build();
        System.out.println("connect");
    }
    public static Connection getInstance() {
        if (instance == null) {
            instance = new Connection();
        }
        return instance;
    }
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
