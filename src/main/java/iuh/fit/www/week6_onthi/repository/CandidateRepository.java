package iuh.fit.www.week6_onthi.repository;

import iuh.fit.www.week6_onthi.db.Connection;
import iuh.fit.www.week6_onthi.model.Candidate;
import iuh.fit.www.week6_onthi.model.CandidateSkill;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class CandidateRepository {
    private final SessionFactory sessionFactory;

    public CandidateRepository() {
        sessionFactory = Connection
                .getInstance()
                .getSessionFactory();
    }

    public List<Candidate> findAll() {
        Transaction transaction = null;
        List<Candidate> candidates = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            candidates = session.createNativeQuery("SELECT * FROM week6.candidate;",Candidate.class).getResultList();
            transaction.commit();
            return candidates;
        } catch (Exception e) {
            transaction.rollback();
            e.printStackTrace();
        }
        return null;
    }
}
