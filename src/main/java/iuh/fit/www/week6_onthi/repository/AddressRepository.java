package iuh.fit.www.week6_onthi.repository;

import iuh.fit.www.week6_onthi.db.Connection;
import iuh.fit.www.week6_onthi.model.Address;
import iuh.fit.www.week6_onthi.model.Candidate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.io.PrintWriter;
import java.util.List;

public class AddressRepository {
    private final SessionFactory sessionFactory;


    public AddressRepository() {
        sessionFactory = Connection.getInstance().getSessionFactory();
    }

    public void insertAddress(Address address) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(address);
    }
    public List<Address> findAll() {
        Transaction transaction = null;
        List<Address> addresses = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            addresses = session.createNativeQuery("SELECT * FROM week6.address;",Address.class).getResultList();
            transaction.commit();
            return addresses;
        } catch (Exception e) {
            transaction.rollback();
            e.printStackTrace();
        }
        return null;
    }
}
