package iuh.fit.www.week6_onthi;
import iuh.fit.www.week6_onthi.db.Connection;
import iuh.fit.www.week6_onthi.model.Address;
import iuh.fit.www.week6_onthi.service.AddressService;
import iuh.fit.www.week6_onthi.service.CandidateService;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "ControlServlet", value = "/")
public class    ControlServlet extends HttpServlet {
    private String message;
    private CandidateService candidateService;
    private AddressService addressService;

    @Override
    public void init() {
        Connection.getInstance().getSessionFactory();
        candidateService = new CandidateService();
        addressService = new AddressService();
        message = "Hello World!";
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getServletPath();

            switch (action) {
                case "/new":
                    showNewForm(request,response);
                    break;
                default:
                    listAddress(request, response);
                    break;
            }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getServletPath();
        switch (action) {
            case "/insert":
                insertAddress(request, response);
                break;
            default:
                listAddress(request, response);
                break;
        }
    }
    private void listAddress(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        List<Address> list = addressService.findAllAddress();
        request.setAttribute("listAddress", list);
        RequestDispatcher dispatcher = request.getRequestDispatcher("address-view.jsp");

        dispatcher.forward(request, response);
    }

    private void showNewForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
        dispatcher.forward(request, response);
    }
    public void destroy() {
    }

    public void insertAddress(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        out.println("Hello word");
        String street = request.getParameter("street");
        String city = request.getParameter("city");
        int country = Integer.parseInt(request.getParameter("country"));
        String number  = request.getParameter("number");
        String zipcode = request.getParameter("zipcode");
        Address address = new Address(street, city, country, number, zipcode);

        addressService.insertAddress(address);
        response.sendRedirect("new");

    }
}
