<%--
  Created by IntelliJ IDEA.
  User: PC
  Date: 10/24/2023
  Time: 2:05 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <i>Fill out the form. Asterisk </i>
    <br>
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>street</th>
                <th>city</th>
                <th>country</th>
                <th>number</th>
                <th>zipcode</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="address" items="${listAddress}">
                <tr>
                    <td>
                        <c:out value="${address.id}"/>
                    </td>
                    <td>
                        <c:out value="${address.street}"/>
                    </td>
                    <td>
                        <c:out value="${address.city}"/>
                    </td>
                    <td>
                        <c:out value="${address.country}"/>
                    </td>
                    <td>
                        <c:out value="${address.number}"/>
                    </td>
                    <td>
                        <c:out value="${address.zipcode}"/>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</body>
</html>
